<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>登录</title>
</head>
<body>
<h1>登录</h1>
<form method="post" action="doLogin">
    <div style="color: red">${msg}</div>
    <p>用户名: <input type="text" name="account" id="account" value="${param.account}"></p>
    <p>密码: <input type="text" name="password" id="password" value="${param.password}"></p>
    <p> <input type="submit" value="登录"></p>
</form>
</body>
</html>
