package com.t365.service.impl;

import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SysUserServiceImplTest {

    @Test
    public void addUser(){
        //SysUserService sysUserService = new SysUserServiceImpl();
        ApplicationContext ctx =new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("userService");
        SysUserService sysUserService2= (SysUserService) ctx.getBean("userService");
        SysUserService sysUserService3 = (SysUserService) ctx.getBean("userService");
        SysUserService sysUserService4 = (SysUserService) ctx.getBean("userService");
        SysUserService sysUserService5 = (SysUserService) ctx.getBean("userService");
//        sysUserService.addSysUser(new SysUser());

        System.out.println(sysUserService2.hashCode());
        System.out.println(sysUserService3.hashCode());
        System.out.println(sysUserService4.hashCode());
        System.out.println(sysUserService5.hashCode());
    }

    @Test
    public void testSpringMybatis(){
        ApplicationContext context =new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        SysUserService userService = (SysUserService) context.getBean("userService");
        userService.addSysUser(new SysUser());
    }

    @Test
    public void testStr(){
        String a="liuqi";
        String b="liqq";
        System.out.println(a.indexOf(b));

    }

}