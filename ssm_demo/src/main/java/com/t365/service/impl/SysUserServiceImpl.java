package com.t365.service.impl;

import com.t365.mapper.SysUserMapper;
import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jane
 * @date 2024-04-22 15:29
 */
@Service("userService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    SysUserMapper sysUserMapper;

    @Transactional
    @Override
    public int addSysUser(SysUser sysUser) {
        return sysUserMapper.addSysUser(sysUser);
    }
}
