package com.t365.service;

import com.t365.pojo.SysUser;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jane
 * @date 2024-04-22 15:29
 */

@Transactional
public interface SysUserService {

    int addSysUser(SysUser sysUser);
}
