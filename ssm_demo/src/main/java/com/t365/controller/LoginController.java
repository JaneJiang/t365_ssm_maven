package com.t365.controller;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Jane
 * @date 2024-04-22 16:30
 */

public class LoginController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("hello springMVC ");
        ModelAndView mv =new ModelAndView();
        mv.addObject("loginUser","周杰");// model: 数据
        //WEB-INF/jsp/login.jsp
        mv.setViewName("/WEB-INF/jsp/login");//view: 页面地址
        return mv;
    }
}
