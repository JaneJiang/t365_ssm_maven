package com.t365.controller;

import com.alibaba.fastjson.JSON;
import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Jane
 * @date 2024-04-24 16:13
 */

@Controller
public class UserAddTest {
    @Autowired
    SysUserService sysUserService;

    @RequestMapping("/saveUser")
    public void addUser(){
        sysUserService.addSysUser(new SysUser());
        throw new RuntimeException("保存用户失败");

    }
    @RequestMapping("/showUser")
    @ResponseBody
    public String tesJson(){
        return JSON.toJSONString(new SysUser(1L,"zhangsan","张三"));
    }

   /*@ExceptionHandler(value = {RuntimeException.class})
    public String handlerExeception(RuntimeException e, HttpServletRequest req){
        req.setAttribute("e",e);
        return "error.jsp";
    }*/
}
