package com.t365.controller;

import com.alibaba.fastjson.JSON;
import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * @author Jane
 * @date 2024-04-22 17:16
 */
@Controller // 单例的
public class UserController
{
     private ArrayList<SysUser> userList= null;
    private ArrayList<SysUser> queryUserList= null;

    public UserController(){
        userList  = new ArrayList<>();
        queryUserList  = new ArrayList<>();

        userList.add(new SysUser(1L,"zhangsan","张三"));
        userList.add(new SysUser(2L,"lisi","李四"));
        userList.add(new SysUser(3L,"zhangxiaowu","张小五"));
        userList.add(new SysUser(4L,"zhaolaoliu","赵老六"));
        userList.add(new SysUser(5L,"liuqi","六七"));

    }

    @RequestMapping("/list")  //http://localhost:8080/ssm_demo/list
    public String list(Model model){
        /*response.setContentType("json/application;charset=UTF-8");
        response.setCharacterEncoding("utf-8");*/
        queryUserList.clear();
        queryUserList.addAll(userList); //放查询结果的
        model.addAttribute("queryUserList",queryUserList);
        return "userlist.jsp";
    }

    @PostMapping("/list")  //post请求
    public String queryList(Model model,@RequestParam String username){   //li

        queryUserList.clear();

        if (null!=username && !"".equals(username)){
            for (SysUser user:userList  ) {
                //zhangsan  zhangchang  zhaoli liuqi
                if (user.getAccount().indexOf(username)!=-1){
                    queryUserList.add(user);
                }
            }
        }
        model.addAttribute("queryUserList",queryUserList);

        return "userlist.jsp";
    }



    @RequestMapping("/login_zc")
    public String login_zc(){
        System.out.println("login_zcccccc");
        //login.jsp
        return "login.jsp";
    }

    @RequestMapping("/doLogin_mv")
    /**
     * 处理用户登录请求
     * @Param HttpServletRequest request 前端请求中的所有的内容
     */
    public ModelAndView doLogin_mv(@RequestParam String account
                        , @RequestParam(value = "password",required = true) String pwd

               ) throws UnsupportedEncodingException {
//        System.out.println(request.getParameter("account"));
//        System.out.println(request.getParameter("password"));

        System.out.println(account);
        System.out.println(pwd);

        ModelAndView mv =new ModelAndView();
        mv.addObject("loginUser",account);
        mv.setViewName("main.jsp");
        //login.jsp
        return mv;
    }
    @RequestMapping("/doLogin")
    /**
     * 处理用户登录请求
     *  jsp-> controller  request   @RequestParam 来传递
     *  controller -> jsp ModelAndView   Model   Map
     */
    public String doLogin(Model model
                          ,@RequestParam String account
                          , @RequestParam(value = "password",required = true) String pwd
    ) {
        String msg ="";
        if ("admin".equals(account)) {
            if ("123456".equals(pwd)){
                model.addAttribute("loginUser",account);
                model.addAttribute("msg","登录成功");
                return "main.jsp";
            }else{
                model.addAttribute("msg","密码错误,登录失败");
            }
        }else{
            model.addAttribute("msg","账号不存在,登录失败");
        }
        return "login.jsp";
    }

}
