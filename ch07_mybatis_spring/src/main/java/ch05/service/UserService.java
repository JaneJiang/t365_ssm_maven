package ch05.service;

public interface UserService {

    public int save();
    public int getUserById(int id);
}
