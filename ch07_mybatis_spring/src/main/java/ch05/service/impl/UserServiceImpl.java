package ch05.service.impl;

import ch05.User;
import ch05.dao.UserDao;
import ch05.dao.impl.UserMysqlDaoImpl;
import ch05.dao.impl.UserRedisDaoImpl;
import ch05.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Jane
 * @date 2024-04-12 16:00
 */
//@Service("userService")
@Service("userService")
public class UserServiceImpl implements UserService {

    //@Resource(name = "userMysqlDao")//userMysqlDao,userRedisDao
   /* @Resource(type = UserRedisDaoImpl.class)
    UserDao userDao ;*/

   // @Resource
    @Autowired
    @Qualifier("userRedisDao")
    UserDao userDao;
    @Override
    public int save() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>userService.save()");
        return userDao.save();
    }

    @Override
    public int getUserById(int id) {
        int i=1/0;
        System.out.println("调用了serviece里面 的查询方法");
        return 365;
    }
}
