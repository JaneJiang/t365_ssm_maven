package ch05;

import org.springframework.stereotype.Component;

/**
 * @author Jane
 * @date 2024-04-12 14:58
 */
@Component("xz")
public class SpringHello {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void show(){
        System.out.println("hello spring: "+ this.getName());
    }
}
