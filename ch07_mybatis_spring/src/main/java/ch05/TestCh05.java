package ch05;

import ch05.dao.UserDao;
import ch05.service.UserService;
import ch05.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author Jane
 * @date 2024-04-12 14:59
 */

public class TestCh05 {
    ApplicationContext ctx =null;
    @Test
    public void testHello(){
       /* SpringHello hello=new SpringHello();
        hello.setName("xiaozhang");
        hello.show();*/

        //加载spring的配置文件
        //控制反转
        //依赖注入
        ApplicationContext ctx =new ClassPathXmlApplicationContext("application_ch05.xml");
        SpringHello hello_xz = (SpringHello) ctx.getBean("hello_xz");
        hello_xz.show();

       /* Random r=new Random();
        int i = r.nextInt(23)+1;
        System.out.println(i);*/


//        ApplicationContext ctx =new ClassPathXmlApplicationContext("application_ch05.xml");
      /*  String[] path = {"classpath:application_ch05.xml","classpath:application_user.xml"};
        ApplicationContext ctx =new FileSystemXmlApplicationContext(path);
        User hahah = (User) ctx.getBean("hahaha");
        System.out.println(hahah.toString());*/

    /*    Dept it = (Dept) ctx.getBean("it");
        System.out.println(it);*/


    }


    @Test
    public void testAnno(){
       /* SpringHello springHello = (SpringHello) ctx.getBean("xz");
        springHello.setName("小张");
        springHello.show();*/

  /*      UserDao userDao = (UserDao) ctx.getBean("userRedisDao");
        userDao.save();*/

        UserService userService = (UserService) ctx.getBean("userService");//new UserServiceImpl();
        userService.save();
    }

    @Before
    public void before(){
        ctx =new ClassPathXmlApplicationContext
                ("application_ch05.xml"
                  ,"application_user.xml"
                  ,"application_test_user.xml"
                );
    }


    @Test
    public void testConstructor(){


        User user = (User)ctx.getBean("zhangchang");
        System.out.println(user);

        //p属性注入
        User xs = (User)ctx.getBean("xuesen");
        System.out.println(xs);

    }
    @Test
    public void testTestUser(){
        TestEntity user = (TestEntity)ctx.getBean("testUser");
        System.out.println(user);

        System.out.println(user.getList().get(1));

    }
}
