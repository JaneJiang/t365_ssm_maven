package ch05;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@ToString
@Data
public class TestEntity {
	private Integer i;
	private int j;
	private String specialCharacter1; // 特殊字符值1
	private String specialCharacter2; // 特殊字符值2
	private User outUser;			  // 引用外部对象
	private User innerBean; 		  // JavaBean类型
	private List<String> list; 		  // List类型
	private String[] array; 		  // 数组类型
	private Set<String> set; 		  // Set类型
	private Map<String, String> map;  // Map类型
	private Properties props; 		  // Properties类型
	private String emptyValue; 		  // 注入空字符串值
	private String nullValue = "init value"; // 注入null值

}
