package ch05;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jane
 * @date 2024-04-12 15:17
 */
@Data
public class Dept implements Serializable {

    private static final long serialVersionUID = 7688349929613936568L;

    private Integer deptId;
    private String deptName;
    private List<User> userList;
    private int i;
}
