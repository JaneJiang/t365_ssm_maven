package ch05.dao.impl;

import ch05.dao.UserDao;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author Jane
 * @date 2024-04-12 15:57
 */
//@Component("userMysqlDao")
@Repository("userMysqlDao")
public class UserMysqlDaoImpl implements UserDao {
    @Override
    public int save() {
        System.out.println(".................mysqlDao.save");
        return 1;
    }
}
