package ch05.dao.impl;

import ch05.dao.UserDao;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author Jane
 * @date 2024-04-12 15:57
 */
//@Component("userRedisDao")
@Repository("userRedisDao")
public class UserRedisDaoImpl implements UserDao {
    @Override
    public int save() {
        System.out.println(".................RedisDao.save");
        return 1;
    }
}
