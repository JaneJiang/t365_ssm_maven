package ch05;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.Data;

/**
 * @author Jane
 * @date 2024-04-12 15:08
 */
@Data
public class User {
    private Integer userId;
    private String userName;
    private Dept dept;

    public User() {
    }

    public User(Integer userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public User(Integer userId, String userName, Dept dept) {
        this.userId = userId;
        this.userName = userName;
        this.dept = dept;
    }
}
