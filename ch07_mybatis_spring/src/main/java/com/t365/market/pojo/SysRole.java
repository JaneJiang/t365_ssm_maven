package com.t365.market.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统角色
 * @TableName t_sys_role
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRole implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String rolename;

    /**
     * 创建者
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改者
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    private static final long serialVersionUID = 1L;
}