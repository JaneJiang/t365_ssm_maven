package com.t365.market.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 系统用户
 * @TableName t_sys_user
 */
@Data
public class SysUser implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 真是姓名
     */
    private String realname;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别（1:女、 2:男）
     */
    private Integer sex;

    /**
     * 出生日期（年-月-日）
     */
    private Date birthday;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户地址
     */
    private String address;

    /**
     * 用户角色id
     */
    private Long roleid;

    /**
     * 创建人
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改人
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    private static final long serialVersionUID = 1L;
}