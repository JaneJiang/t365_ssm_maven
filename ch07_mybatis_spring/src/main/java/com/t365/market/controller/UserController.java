package com.t365.market.controller;

import com.t365.market.pojo.SysRole;
import com.t365.market.service.SysRoleService;
import com.t365.market.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author Jane
 * @date 2024-04-19 16:24
 */
@Component
public class UserController {

    @Autowired
    @Qualifier("userServiceMapper")
    SysUserService userService;

    @Autowired
    @Qualifier("sysRoleService")
    SysRoleService roleService;

    @Transactional
    public void updateUserInfo(SysRole role){

        userService.updateRole();//改id为17的用户的角色为3
        int i =1/0;
        roleService.updateRole(role);//根据传入的id修改角色名称

    }
}
