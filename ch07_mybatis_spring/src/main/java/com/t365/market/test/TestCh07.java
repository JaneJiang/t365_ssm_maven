package com.t365.market.test;

import com.t365.market.controller.UserController;
import com.t365.market.pojo.SysRole;
import com.t365.market.service.SysRoleService;
import com.t365.market.service.SysUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author Jane
 * @date 2024-04-17 15:17
 */

public class TestCh07 {

    @Test
    public void testMybatis() throws IOException {
     /*   SysUserService userService = new SysUserServiceImpl();
        userService.getCount();*/

        ApplicationContext ctx = new ClassPathXmlApplicationContext("application_ch07.xml");
        SysUserService userService = (SysUserService) ctx.getBean("userService"); // new SysUserServiceImpl();
        userService.getCount_dao();
    }

    @Test
    public void testMybatis_MapperFactoryBean() throws IOException {
     /*   SysUserService userService = new SysUserServiceImpl();
        userService.getCount();*/

        ApplicationContext ctx = new ClassPathXmlApplicationContext("application_ch07.xml");
        SysUserService userService = (SysUserService) ctx.getBean("userService"); // new SysUserServiceImpl();
        userService.getCount_dao();

        //userServiceMapper
        SysUserService userServiceMapper = (SysUserService) ctx.getBean("userServiceMapper");
        userServiceMapper.getUserCount_mapper();
    }

    @Test
    public void testMybatis_roleService() throws IOException {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("application_ch07.xml");
        SysRoleService roleService= (SysRoleService) ctx.getBean("sysRoleService"); // new SysUserServiceImpl();
        roleService.getRoleNameById(1);
    }


    @Test
    public void updateRole() throws IOException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application_ch07.xml");
        SysUserService userService = (SysUserService)ctx.getBean("userServiceMapper") ;
        SysRoleService roleService= (SysRoleService) ctx.getBean("sysRoleService"); // new SysUserServiceImpl();

        SysRole role = new SysRole();
        role.setId(3L);
        role.setRolename("店员");
        roleService.updateRole(role);

        //把角色名称改了一下
        //用户的角色重新绑定了一下
        userService.updateRole();


    }

    @Test
    public void updateRole_controller() throws IOException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application_ch07.xml");

        SysRole role = new SysRole();
        role.setId(3L);
        role.setRolename("zhangchang");

        UserController controller = (UserController) ctx.getBean("userController");
        controller.updateUserInfo(role);




    }
}
