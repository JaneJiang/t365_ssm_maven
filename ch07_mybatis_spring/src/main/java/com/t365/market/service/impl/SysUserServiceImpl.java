package com.t365.market.service.impl;

import com.t365.market.dao.SysUserDao;
import com.t365.market.dao.SysUserMapper;
import com.t365.market.service.SysUserService;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * @author Jane
 * @date 2024-04-17 15:16
 */

@Transactional
public class SysUserServiceImpl implements SysUserService {

    SysUserDao userDao;

    SysUserMapper userMapper;



    public SysUserMapper getUserMapper() {
        return userMapper;
    }

    public void setUserMapper(SysUserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public SysUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(SysUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public int getCount_dao() throws IOException {
        return userDao.getCount();
    }

    @Override
    public int getUserCount_mapper() {
        return userMapper.getUserCount();
    }

    @Override
    public int updateRole() {
        return userMapper.updateRole();
    }
}
