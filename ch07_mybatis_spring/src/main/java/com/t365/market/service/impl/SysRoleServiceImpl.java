package com.t365.market.service.impl;

import com.t365.market.dao.SysRoleMapper;
import com.t365.market.pojo.SysRole;
import com.t365.market.service.SysRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Transactional
@Service("sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

//    @Autowired
    @Resource
    SysRoleMapper sysRoleMapper;

    public String getRoleNameById(Integer roleId){
        return sysRoleMapper.getRoleNameById(roleId);
    }

    @Override
    public int updateRole(SysRole role) {
        return sysRoleMapper.updateRole(role);
    }
}
