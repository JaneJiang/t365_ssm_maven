package com.t365.market.service;

import com.t365.market.pojo.SysRole;

public interface SysRoleService {
    public String getRoleNameById(Integer roleId);

    public int updateRole(SysRole role);
}
