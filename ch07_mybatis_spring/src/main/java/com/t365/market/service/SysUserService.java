package com.t365.market.service;

import java.io.IOException;

public interface SysUserService {

    public int getCount_dao() throws IOException;

    public int getUserCount_mapper();
    public int updateRole();

}
