package com.t365.market.dao.impl;

import com.t365.market.dao.SysUserDao;
import org.mybatis.spring.SqlSessionTemplate;

import java.io.IOException;

/**
 * @author Jane
 * @date 2024-04-17 16:08
 */

public class SysUserDaoImpl implements SysUserDao {

    //p162 示例6
    private SqlSessionTemplate session;  //加载mybatis_config.xml映射文件 获取数据库连接

    @Override
    public int getCount() throws IOException {
       /* //1. 加载mybatis_config.xml映射文件
        String url ="mybaits-config.xml";
        InputStream is = Resources.getResourceAsStream(url);

        SqlSession session =null;
        SqlSessionFactory fac= new SqlSessionFactoryBuilder().build(is);

        session =fac.openSession();
        int count  = session.getMapper(SysUserDao.class).getCount();
        System.out.println("总数: "+count);

        session.close();
*/
        return session.getMapper(SysUserDao.class).getCount();
    }

    public SqlSessionTemplate getSession() {
        return session;
    }

    public void setSession(SqlSessionTemplate session) {
        this.session = session;
    }
}
