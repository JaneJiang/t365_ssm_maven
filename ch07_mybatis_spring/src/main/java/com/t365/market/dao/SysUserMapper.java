package com.t365.market.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author Jane
 * @date 2024-04-17 16:53
 */

public interface SysUserMapper {

    @Select("select 365 ")
    public int getUserCount();


    @Update("update t_sys_user set roleId=1 where id=16")
    public int updateRole();
}
