package com.t365.market.dao;

import com.t365.market.pojo.SysRole;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleMapper {

    @Select("select roleName from t_sys_role where id= #{roleId}")
    public String getRoleNameById(Integer roleId);

    @Update("update t_sys_role set roleName = #{rolename} where id=#{id} ")
    public int updateRole(SysRole role);
}
