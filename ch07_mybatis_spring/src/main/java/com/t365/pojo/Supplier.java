package com.t365.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 药品供货商
 * @TableName t_supplier
 */
@Data
public class Supplier implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 供货商编号
     */
    private String supcode;

    /**
     * 供货商名称
     */
    private String supname;

    /**
     * 供货商描述
     */
    private String supdesc;

    /**
     * 供货商联系人
     */
    private String supcontact;

    /**
     * 联系电话
     */
    private String supphone;

    /**
     * 供货商地址
     */
    private String supaddress;

    /**
     * 传真
     */
    private String supfax;

    /**
     * 创建人id
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 修改人id
     */
    private Long updateduserid;

    private static final long serialVersionUID = 1L;
}