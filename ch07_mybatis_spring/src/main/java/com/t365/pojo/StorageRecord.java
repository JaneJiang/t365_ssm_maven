package com.t365.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * 入库记录
 * @TableName t_storage_record
 */
@Data
public class StorageRecord implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 入库记录编码
     */
    private String srcode;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品描述
     */
    private String goodsdesc;

    /**
     * 商品单位
     */
    private String goodsunit;

    /**
     * 入库数量
     */
    private BigDecimal goodscount;

    /**
     * 入库商品总额
     */
    private BigDecimal totalamount;

    /**
     * 支付状态（1：未支付 2：已支付）
     */
    private Integer paystatus;

    /**
     * 创建人id
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改人id
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 供货商ID
     */
    private Long supplierid;

    private static final long serialVersionUID = 1L;
}