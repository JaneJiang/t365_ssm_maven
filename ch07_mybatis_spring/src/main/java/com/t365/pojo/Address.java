package com.t365.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_address
 */
@Data
public class Address implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 联系人姓名
     */
    private String contact;

    /**
     * 收货地址明细
     */
    private String addressdesc;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 联系人电话
     */
    private String tel;

    /**
     * 创建者
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改者
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    /**
     * 用户ID
     */
    private Long userid;

    private static final long serialVersionUID = 1L;
}