package ch06.test;

import ch05.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Jane
 * @date 2024-04-15 16:46
 */

public class TestCh06 {
    ApplicationContext ctx =null;
    @Before
    public void before(){
        ctx= new ClassPathXmlApplicationContext("application_ch05.xml","application_ch06_aop.xml");
    }
    @Test
    public void testSave(){
        UserService userService = (UserService) ctx.getBean("userService");
        //userService.save();
        userService.getUserById(80);

    }
}
