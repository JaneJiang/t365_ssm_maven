package ch06.logger;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
//import org.junit.Before;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Jane
 * @date 2024-04-15 16:22
 */
@Component
@Aspect
public class UserServiceLogger {
    @Pointcut(value = "execution(* ch05.service.*.query*(..))")
    public void pointcut(){}


    @Before(value="pointcut()")
    //@Before(value ="execution(* ch05.service.*.*(..))")
    public void before(JoinPoint jp){
        System.out.println("前置增强--调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法"+ Arrays.toString(jp.getArgs()));
        System.out.println("添加数据之前,先开启事务");
    }

    //返回后通知 ,无论是否发生异常都会执行
    @After(value="pointcut()")
    public void end(JoinPoint jp){
        System.out.println("后置增强--调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法");
        System.out.println("添加数据之后,提交事务");
     //   return  result;
    }

    @AfterThrowing(value = "execution(* ch05.service.*.get*(..))",throwing = "ex")
    public void exeception(JoinPoint jp,RuntimeException ex){
        System.out.println("==============异常增强");

        System.out.println("异常消息: "+ex.getMessage());

    }


}
