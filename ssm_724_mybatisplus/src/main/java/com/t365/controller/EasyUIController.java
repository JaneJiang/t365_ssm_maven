package com.t365.controller;

import com.alibaba.fastjson.JSON;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysUser;
import com.t365.pojo.TreeResult;
import com.t365.service.TreeResultService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Jane
 * @date 2024-05-10 15:34
 */

@Controller
@RequestMapping("/easyUi")
public class EasyUIController {

    @Resource
    TreeResultService treeResultService;

    @RequestMapping("/toTree")
    public String tree(){
        return "tree.jsp";
    }

    ///user/tolist
    @RequestMapping("/user/tolist")
    public String toUserList(){
        return "/ui/userList.jsp";
    }

    @CrossOrigin
    @RequestMapping("/getTreeList")
    @ResponseBody
    public List<TreeResult> getTreeList(@RequestParam(defaultValue = "0") String id,
                                        HttpSession session){
       // SysUser user= (SysUser)  session.getAttribute("userSession");//userSession
        Integer rid =1;//user.getRoleid().intValue();
        List<TreeResult> menus = treeResultService.getMenuListByRidAndParentId(rid, id);
        return menus;
    }



}
