package com.t365.controller;

import com.alibaba.fastjson.JSON;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysUser;
import com.t365.service.SysMenuService;
import org.apache.http.protocol.HTTP;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Jane
 * @date 2024-05-08 16:35
 */

@Controller
//@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    SysMenuService sysMenuService;

    @RequestMapping("/menuList")
    @ResponseBody //ajax
    @CrossOrigin
    public String menuList(@RequestParam("parentId") String parentId, HttpSession session){
        SysUser sysUser = (SysUser) session.getAttribute("userSession");//获取用户的角色id
        List<SysMenu> menuList = sysMenuService.getMenuListByRidAndParentId(sysUser.getRoleid().intValue(), parentId);

        String menuList_json = JSON.toJSONString(menuList);
        session.setAttribute("menuList_json",menuList_json);

        return menuList_json; //加上@ResponseBody注解之后,string类型的字符串会直接返回到浏览器 ,不会再被解析成视图解析器

    }
}
