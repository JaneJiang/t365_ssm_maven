package com.t365.controller;

import com.t365.utils.FileUploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Jane
 * @date 2024-04-22 16:30
 */
@Controller
public class LoginController {

    @RequestMapping({"/login"})
    public String login(){
        return "login.jsp";
    }  //页面跳转

    @RequestMapping("/uploadImg")
    public String fileUpload(){
        return "/sysUser/update_file.jsp";
    }

    @RequestMapping("/doUpoadImg")
    @ResponseBody  //ajax
    public String doUpoadImg(@RequestParam(value = "attach",required = false) MultipartFile attachs) throws IOException {
        return FileUploadUtil.uploadImage(attachs); //返回url
    }
}
