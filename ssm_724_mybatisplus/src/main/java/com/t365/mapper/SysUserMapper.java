package com.t365.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.t365.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select("select 365")
    int addSysUser(SysUser sysUser);

    //条件查询
    //写自定义sql
/*    public List<SysUser> getAllByRealnameAndRoleid(@Param("realName") String realname
                                                   , @Param("roleId") String roleId
                                                    );*/

    public Page<SysUser> getAllByRealnameAndRoleid(@Param("realName") String realname
                                                    , @Param("roleId") String roleId
                                                    , @Param("page") Page page
    );

}




