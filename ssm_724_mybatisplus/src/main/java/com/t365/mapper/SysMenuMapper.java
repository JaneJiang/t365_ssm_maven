package com.t365.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Jane
 * @date 2024-04-28 15:56
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    @Select("SELECT * FROM `t_sys_menu` where 1=1 " +
            " and menu_Id in(select menuId from r_role_menu where rid= #{roleId}) " +
            " and parent_id=#{parentId} " +
            " and is_deleted=0 ")
    public List<SysMenu> getMenuListByRidAndParentId(@Param("roleId") Integer roleId
                                                     ,@Param("parentId") String parentId);

}
