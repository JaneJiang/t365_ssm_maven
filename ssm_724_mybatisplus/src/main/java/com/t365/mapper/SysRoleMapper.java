package com.t365.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.t365.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Jane
 * @date 2024-04-28 15:56
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
