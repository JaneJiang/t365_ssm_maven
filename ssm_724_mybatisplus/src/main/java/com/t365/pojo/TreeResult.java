package com.t365.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Jane
 * @date 2024-05-08 10:07
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeResult {
    private Integer id;
    private String text;
    private String state;
    private Map<String,Object> attributes;//存储自定义数据
}
