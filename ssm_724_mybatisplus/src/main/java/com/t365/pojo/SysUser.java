package com.t365.pojo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 系统用户
 * @TableName t_sys_user
 */
@Data
@Scope("prototype")
@TableName(value ="t_sys_user")
public class SysUser implements Serializable {
    /**
     * 主键ID
     */
   // @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 真是姓名
     */
    private String realname;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别（1:女、 2:男）
     */
    private Integer sex;

    /**
     * 出生日期（年-月-日）
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户地址
     */
    private String address;

    /**
     * 用户角色id
     */
    private Long roleid;

    /**
     * 创建人
     */
    private Long createduserid;

    /**
     * 创建时间
     */
    private Date createdtime;

    /**
     * 修改人
     */
    private Long updateduserid;

    /**
     * 修改时间
     */
    private Date updatedtime;

    @TableField(exist = false) //忽略数据库字段映射
    private String roleName;
    @TableField(exist = false)
    private Integer age;

    private String headerpic;


    private static final long serialVersionUID = 1L;

    public SysUser(){}

    public SysUser(Long id, String account, String realname) {
        this.id = id;
        this.account = account;
        this.realname = realname;
    }
}