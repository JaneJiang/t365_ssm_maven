package com.t365.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 系统菜单
 * @author Jane
 * @date 2024-05-08 15:47
 */

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_sys_menu")
public class SysMenu {
    @TableId
    private Integer menu_id;
    private String menu_name;
    private String url;
    private Integer parent_id;
    private String menu_desc;
    private Integer is_parent;  //是否是一级菜单  layui用的
    private Integer is_deleted;  //是否删除

}
