package com.t365.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author Jane
 * @date 2024-04-28 15:51
 */

@Data
@ToString
@TableName("t_sys_role")
public class SysRole {
    @TableId
    private long id;
    private String code;
//    @TableField(value ="roleName" )
    private String roleName;  //数据库字段 role_name
    /**
     * 创建人
     */
    private Long createduserid;
    /**
     * 创建时间
     */
    private Date createdtime;
    /**
     * 修改人
     */
    private Long updateduserid;
    /**
     * 修改时间
     */
    private Date updatedtime;

}
