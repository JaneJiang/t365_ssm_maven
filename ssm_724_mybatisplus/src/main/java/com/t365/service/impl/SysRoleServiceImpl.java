package com.t365.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.t365.mapper.SysRoleMapper;
import com.t365.pojo.SysRole;
import com.t365.service.SysRoleService;
import org.springframework.stereotype.Service;

/**
 * @author Jane
 * @date 2024-04-28 15:57
 */
@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
}
