package com.t365.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.t365.mapper.SysUserMapper;
import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Jane
 * @date 2024-04-22 15:29
 */
@Service("userService")
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    SysUserMapper sysUserMapper;
    @Override
    public int addSysUser(SysUser sysUser) {
        return sysUserMapper.addSysUser(sysUser);
    }

    @Override
    public SysUser doLogin(String account, String password) {
        QueryWrapper<SysUser> wrapper =new QueryWrapper<>();//map
        wrapper.eq("account",account);
        wrapper.eq("password",password);
        //条件查询
        return sysUserMapper.selectOne(wrapper);
    }

    @Override
    public List<SysUser> getAllByRealnameAndRoleid(String realname, String roleId) {
        return null;//sysUserMapper.getAllByRealnameAndRoleid(realname,roleId);
    }

    @Override
    public Page<SysUser> getAllByRealnameAndRoleid(String realname, String roleId, Page page) {
        return sysUserMapper.getAllByRealnameAndRoleid(realname,roleId,page);
    }
}
