package com.t365.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.t365.mapper.SysUserMapper;
import com.t365.pojo.SysUser;
import com.t365.service.SysUserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class SysUserServiceImplTest {
    SysUserService userService =null;
    @Before
    public void before(){
        ApplicationContext ctx =new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        userService=(SysUserService) ctx.getBean("userService");
    }


    @Test
    public void testZhangsan(){
        ApplicationContext ctx =new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        //SysUserMapper sysUserMapper = (SysUserMapper) ctx.getBean("sysUserMapper");
//        sysUserMapper.addSysUser(new SysUser());
        SysUserService userService = (SysUserService) ctx.getBean("userService");
        userService.addSysUser(new SysUser());
        userService.list(); //调用mybatisplus查全部的方法
    }

    @Test
    public void doLogin(){
        String account="admin";
        String password="1111112";
        //条件查询
        userService.doLogin(account,password);
    }

    @Test
    public void getAllByRealnameAndRoleid(){
        String realName="刘";
        String roleId ="2";
        //条件查询
        userService.getAllByRealnameAndRoleid(realName,roleId);
    }

    @Test
    public void testIPage(){ //MybatisPlus 分页插件
        Page<SysUser> page =new Page<>(1,5);
        //userService.page(page,null);
        userService.getAllByRealnameAndRoleid("刘","2",page);
    }

}