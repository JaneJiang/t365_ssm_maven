package com.t365.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.t365.mapper.SysMenuMapper;
import com.t365.mapper.SysRoleMapper;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysRole;
import com.t365.service.SysMenuService;
import com.t365.service.SysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Jane
 * @date 2024-05-08 16:13
 */

@Service("sysMenuService")
public class SysMenuServiceImpl  extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Resource
    SysMenuMapper sysMenuMapper;
    @Override
    public List<SysMenu> getMenuListByRidAndParentId(Integer roleId, String parentId) {
        return sysMenuMapper.getMenuListByRidAndParentId(roleId,parentId);
    }
}
