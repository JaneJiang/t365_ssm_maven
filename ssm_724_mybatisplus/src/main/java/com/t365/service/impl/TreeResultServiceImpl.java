package com.t365.service.impl;

import com.t365.pojo.SysMenu;
import com.t365.pojo.TreeResult;
import com.t365.service.SysMenuService;
import com.t365.service.TreeResultService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jane
 * @date 2024-05-10 15:21
 */

@Service("treeResultService")
public class TreeResultServiceImpl implements TreeResultService {
    @Resource
    SysMenuService sysMenuService;

    @Override
    public List<TreeResult> getMenuListByRidAndParentId(Integer roleId, String parentId) {
        //从数据库查出来符合条件的数据
        List<SysMenu> menuList = sysMenuService.getMenuListByRidAndParentId(roleId, parentId);
        //转换成List<TreeResult>

        List<TreeResult> treeResList= new ArrayList<>();
        TreeResult treeResult =null;
        Map<String,Object> attrMaps=null;

        for (SysMenu menu: menuList ) {
            treeResult =new TreeResult();
            attrMaps= new HashMap<>();

            treeResult.setId(menu.getMenu_id());
            treeResult.setText(menu.getMenu_name());
            treeResult.setState(menu.getIs_parent()==1?"closed":"open");//1级菜单默认关闭

            attrMaps.put("url",menu.getUrl());
            attrMaps.put("isParent",menu.getIs_parent());

            treeResult.setAttributes(attrMaps);
            treeResList.add(treeResult);
        }

        return treeResList;
    }
}
