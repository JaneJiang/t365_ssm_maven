package com.t365.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysRole;
import com.t365.service.SysMenuService;
import com.t365.service.SysRoleService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SysRoleServiceImplTest {

    @Test
    public void testRole(){
        ApplicationContext ctx= new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        SysRoleService sysRoleService = (SysRoleService) ctx.getBean("sysRoleService");
        //查全部
        //sysRoleService.list();
        //sysRoleService.getById(1);
        //sysRoleService.listByIds(Arrays.asList(3,2));
        String roleName ="店";
        QueryWrapper<SysRole> queryWrapper =new QueryWrapper();
        queryWrapper.like("roleName",roleName);
        sysRoleService.list(queryWrapper);
    }

    @Test
    public void testMenu(){
        ApplicationContext ctx= new ClassPathXmlApplicationContext("applicationContext-jdbc.xml");
        SysMenuService sysMenuService = (SysMenuService) ctx.getBean("sysMenuService");
        List<SysMenu> menus = sysMenuService.getMenuListByRidAndParentId(3, "0");
        System.out.println(JSON.toJSONString(menus));

    }
}