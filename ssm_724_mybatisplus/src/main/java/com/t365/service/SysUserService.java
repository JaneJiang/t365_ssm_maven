package com.t365.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.t365.pojo.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Jane
 * @date 2024-04-22 15:29
 */

@Transactional
public interface SysUserService extends IService<SysUser> {

    int addSysUser(SysUser sysUser);

    public SysUser doLogin(String account,String password);

    public List<SysUser> getAllByRealnameAndRoleid(String realname, String roleId);
    public Page<SysUser> getAllByRealnameAndRoleid(String realname, String roleId, Page page);
}
