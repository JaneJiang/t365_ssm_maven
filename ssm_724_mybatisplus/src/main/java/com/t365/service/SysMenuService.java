package com.t365.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.t365.pojo.SysMenu;
import com.t365.pojo.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMenuService extends IService<SysMenu> {
    public List<SysMenu> getMenuListByRidAndParentId(Integer roleId,String parentId);
}
