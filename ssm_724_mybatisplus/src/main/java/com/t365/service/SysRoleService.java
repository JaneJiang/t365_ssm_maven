package com.t365.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.t365.pojo.SysRole;

public interface SysRoleService extends IService<SysRole> {
}
