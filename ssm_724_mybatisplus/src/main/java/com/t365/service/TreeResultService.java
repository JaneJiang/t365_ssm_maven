package com.t365.service;

import com.t365.pojo.SysMenu;
import com.t365.pojo.TreeResult;

import java.util.List;

public interface TreeResultService {
    public List<TreeResult> getMenuListByRidAndParentId(Integer roleId, String parentId);
}
