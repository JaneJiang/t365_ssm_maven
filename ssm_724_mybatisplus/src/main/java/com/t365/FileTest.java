package com.t365;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author Jane
 * @date 2024-05-06 16:31
 */

public class FileTest {

    @Test
    public void testUpdateOss() {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-shenzhen.aliyuncs.com/";
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = "LTAI5t6PGQ6s9NUztziUVvZA";
        String accessKeySecret = "w1OxbLjIawV48U15t3OZNr3lIsYEig";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("C:\\Users\\Tom\\Pictures\\icons8_house_240px.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ossClient.putObject("test-ssm-file", "icons8_house_365.png", inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();
    }
}
