package com.t365.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Jane
 * @date 2024-05-06 16:50
 */

public class FileUploadUtil {
    //注意最后面的/ 要加上 否则地址栏串文件名时会少一个/
    private static final String ALI_DOMAIN="http://test-ssm-file.oss-cn-shenzhen.aliyuncs.com/";

    public static String uploadImage(MultipartFile file) throws IOException {

        String originalFileName = file.getOriginalFilename();
        String ext = "."+ FilenameUtils.getExtension(originalFileName);
        String uuid = UUID.randomUUID().toString().replace("-","");
        String fileName  = uuid + ext;

        String endpoint ="http://oss-cn-shenzhen.aliyuncs.com";
        String accessKeyId = "LTAI5t6PGQ6s9NUztziUVvZA";
        String accessKeySecret = "w1OxbLjIawV48U15t3OZNr3lIsYEig";

        OSS ossClient = new OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);

        //传输
        ossClient.putObject("test-ssm-file",fileName,file.getInputStream());

        //关闭
        ossClient.shutdown();

        //http://test-ssm-file.oss-cn-shenzhen.aliyuncs.com/3a3605e64d5544348d7c9e96bdfa8c25.png
        return ALI_DOMAIN+fileName;
    }
}
