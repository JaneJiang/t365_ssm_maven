<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>724便利店管理系统</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/statics/css/style.css" />
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/statics/css/public.css" />
</head>
<body>
<!--头部-->
    <header class="publicHeader">
        <h1>724便利店管理系统</h1>
        <div class="publicHeaderR">
            <p><span style="color: #fff21b"> ${userSession.realname }</span> , 欢迎光临！</p>
            <a href="${pageContext.request.contextPath }/jsp/logout.do">登出</a>
        </div>
    </header>
<!--时间-->
    <section class="publicTime">
        <span id="time">2019年1月1日 11:11  星期一</span>
        <a href="#">为了保证正常使用，请使用IE10.0以上版本！</a>
    </section>

<script type="text/javascript">

    function showSubMenu(parentId,obj) {

        $.ajax({
            type:"GET",
            url:path+"/menu/menuList",  //${pageContext.request.contextPath }
            data:{parentId:parentId},
            dataType:"json",
            success:function(data){
                console.log(data);//json??  list?

                var subMenu=`<ul>`;
                //解析 追加到左边菜单
                $(data).each(function (index,item) {
                  //  console.log(index)
                    subMenu+= `<li><a href= "\${path}\${this.url}" >\${item.menu_name}</a></li>`
                });
                subMenu +=`</ul>`;
              /*  var parent = $(obj).parent().html();
                $(obj).parent().html(parent+subMenu);*/

                $(obj).parent().append(subMenu);
            },
            error:function(data){
                alert("对不起，信息获取失败");
            }
        });
    }

</script>

 <!--主体内容-->
 <section class="publicMian">
     <div class="left">
         <h2 class="leftH2"><span class="span1"></span>菜单 <span></span></h2>
         <nav>
             <ul class="list">
            <%--  <li ><a href="${pageContext.request.contextPath }/jsp/storageRecord.do?method=query">入库记录信息</a></li>
              <li><a href="${pageContext.request.contextPath }/jsp/supplier.do?method=query">供货商信息</a></li>
              <li><a href="${pageContext.request.contextPath }/user/list">用户信息</a></li>
              <li><a href="${pageContext.request.contextPath }/jsp/sysUser/updatePassword.jsp">修改密码</a></li>
              <li><a href="${pageContext.request.contextPath }/jsp/logout.do">退出系统</a></li>--%>
                 <c:forEach items="${menuList}" var="menu">
                     <li>
                         <a href="javascript:void(0)" onclick="showSubMenu(${menu.menu_id},this)">${menu.menu_name}</a>
                     </li>
                 </c:forEach>
                <%--${pageContext.request.contextPath }/${menu.url}--%>
             </ul>
         </nav>
     </div>
     <input type="hidden" id="path" name="path" value="${pageContext.request.contextPath }"/>
     <input type="hidden" id="referer" name="referer" value="<%=request.getHeader("Referer")%>"/>
  <!-- </section> -->