<%--
  Created by IntelliJ IDEA.
  User: Tom
  Date: 2024-05-10
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户列表</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/statics/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/statics/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/statics/css/demo.css">
    <script type="text/javascript" src="${pageContext.request.contextPath }/statics/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/statics/js/jquery.easyui.min.js"></script>
</head>
<body>
<div id="tb" style="padding:3px">
    <span>账号:</span>
    <input id="queryRealName" name="queryRealName" style="line-height:26px;border:1px solid #ccc">
    <span>角色:</span>
    <!-- <input id="queryRoleId" name="queryRoleId" style="line-height:26px;border:1px solid #ccc"> -->
    <input id="queryRoleId" name="queryRoleId"  class="easyui-combobox" name="role"
           data-options="valueField:'id',textField:'roleName'">

    <a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()">查询</a>
</div>

<table id="dg" title="用户信息列表" style="width:700px;height:300px" data-options="
				rownumbers:true,
				singleSelect:true,
				autoRowHeight:false,
				pagination:true,
				pageSize:10,
                pageList:[5,10,15,20]
                ">
    <thead>
    <tr>
        <th field="id" width="80">id</th></th>
        <th field="account" width="100">账户</th>
        <th field="realName" width="80">姓名</th>
        <th field="roleName" width="80" align="center">角色</th>
        <th field="phone" width="110">联系方式</th>
    </tr>
    </thead>
</table>
<script>

    function doSearch() {

        $.getJSON("http://localhost:8080/ssm_724/layui/user/list"
            ,{queryRealName:$('#queryRealName').val(),queryRoleId:$('#queryRoleId').val()}
            ,function(userList){
                console.log(userList);
                $('#dg').datagrid().datagrid('loadData', userList.rows);
            });
    }

    function pagerFilter(data){
        if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
            data = {
                total: data.length,
                rows: data
            }
        }
        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            onSelectPage:function(pageNum, pageSize){
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh',{
                    pageNumber:pageNum,
                    pageSize:pageSize
                });
                dg.datagrid('loadData',data);
            }
        });
        if (!data.originalRows){
            data.originalRows = (data.rows);
        }
        var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));
        return data;
    }

    $(function(){
        $.getJSON("http://localhost:8080/ssm_724/layui/user/list",function(userList){
            $('#dg').datagrid({loadFilter:pagerFilter}).datagrid('loadData', userList.rows);
        });
        $.getJSON("http://localhost:8080/ssm_724/layui/user/roleList",function(data){
            console.log(data);
            $('#queryRoleId').combobox({'data': data});
        });

        // $('#dg').datagrid({loadFilter:pagerFilter}).datagrid('loadData', getData());
    });
</script>
</body>
</html>