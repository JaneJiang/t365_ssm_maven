<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/common/head.jsp"%>

<script src="${pageContext.request.contextPath }/statics/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function (){
        $("#header1").change(function (){
            // alert($(this).val());
            var fileUpload = this.files[0];
            console.log(fileUpload.name);
            console.log(fileUpload.size); //byte 的单位值  ->KB   -> MB  ->GB
            //要限制文件大小只能是2M以内
            if (fileUpload.size>1024*1024*2){
                alert("非VIP只能上传2MB以内的文件")
                return ;
            }
            //图片格式的限定
            if(!(/\.jpg$|\.jpeg$|\.png$|\.gif$/i.test(fileUpload.name))){
                alert("只能上传.jpg,.jpeg,.png.gif格式的图片");
                return ;
            }
            //$("previewImg").attr("src","?");
            document.getElementById("previewImg").src=window.URL.createObjectURL(fileUpload);
        });
    });
</script>
    });
</script>

    <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>用户管理页面 >> 用户修改页面</span>
        </div>
        <div class="supplierAdd">
        <form id="userForm" name="userForm" method="post"
              ENCTYPE="multipart/form-data"
              action="${pageContext.request.contextPath }/doUpoadImg"
        >

            <div>
                <label>头像：</label>
                <input type="file" name="attach" id="header1">
                <img width="30px" height="30px" id="previewImg">

            </div>
			 <div class="supplierAddBtn">
                    <input type="submit" name="save" id="save" value="提交" />
                </div>
            </form>
        </div>
    </div>
</section>
<%@include file="/WEB-INF/jsp/common/foot.jsp" %>

