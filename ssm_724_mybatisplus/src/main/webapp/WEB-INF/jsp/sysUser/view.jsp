<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/common/head.jsp"%>
 <div class="right">
        <div class="location">
            <strong>当前位置:</strong>
            <span>用户管理页面 >> 用户查看详情页面</span>
        </div>
        <div class="supplierView">
            <p><strong>用户编号：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>${sysUser.account }</span></p>
            <p><strong>账号：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>${sysUser.realname }</span></p>
            <p><strong>性别：</strong>
            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	<span>
            		<c:if test="${sysUser.sex == 1 }">男</c:if>
					<c:if test="${sysUser.sex == 2 }">女</c:if>
				</span>
			</p>
            <p><strong>出生日期：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><fmt:formatDate value="${sysUser.birthday }" pattern="yyyy-MM-dd"/></span></p>
            <p><strong>手机号码：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>${sysUser.phone }</span></p>
            <p><strong>地址：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>${sysUser.address }</span></p>
            <p><strong>角色：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>${sysUser.roleName}</span></p>

            <p><strong>头像：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>
                ${pageContext.request.contextPath }${sysUser.headerpic} <br>
            ${pageContext.request.contextPath }/statics/fileuploads/819ea1a519d34590927fcb24a060d95e.png</span>

                <img width="50px" height="50px"
                     src="${pageContext.request.contextPath }/statics/uploadfiles/819ea1a519d34590927fcb24a060d95e.png">
                <img width="50px" height="50px"
                        src="https://test-ssm-file.oss-cn-shenzhen.aliyuncs.com/23a8cad7caec43179502e3bb14d74eb8.png?Expires=1714982483&OSSAccessKeyId=TMP.3KguRh7NnjfPuA16tUyTRnH9fAZ3N537dz9JTFiArRGYStBRbeT7rv8Ljm4qV7haUQeuH6VfuNaTgu3czRxYU22iNbK35e&Signature=64EZuT%2F9EzyMU2np8zhCCSmgWzI%3D">
            </p>
			<div class="supplierAddBtn">
            	<input type="button" id="back" name="back" value="返回" >
            </div>
        </div>
    </div>
</section>
<%@include file="/WEB-INF/jsp/common/foot.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath }/statics/js/sysUser/view.js"></script>